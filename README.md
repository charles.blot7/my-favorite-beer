# 🍻 My favorite beer 🍻

<div align="center" width="100%">
<img src="frontend/src/assets/images/beer.jpg" width="20%" alt="Project image">
</div>

---

## Goal of the kata

#### Have you ever forgot your **beers taste**  ? Have you ever drunk too much until forgotting beers marks ?

- You're gonna deploy a simple 3tier application that referenced Beers 🍻 and rates associated 
- The goal of this kata is to automate the deployment of the front/back application, building the pipeline and deploy it in a Kubernetes Cluster
- You're going to be challenged in your pipeline efficiency, lifecycle choices and result. 
- Don't be afraid , there is multiple levels and do what you can. Higher the level you completed, better DevOps you are.
- Don't try to hide the postgrl credentials, this kata is focused on lifecycle.

## Informations

- The backend part ( Spring application ) is under backend directory. You must configure the postgrl connection inside. 
> ❗ Carefull to CORS policies between applications !
- The frontend part ( Angular application ) is under frontend directory. 
> 💡 The connection to spring application is **templated** but for **local testing** you can modify the url.
- The kubernetes part ( Helm charts ) is under k8-chart. 
> 💡 Keep the templates **as they are** but you need to populate the variables to customize the templates following the gitlab-ci deployment.
- stages
    * **app_build** (Compile the Angular and NodeJS and ensure the developpers didn't inserted deployment bugs) => These steps create Artifacts
    * **image_build** (Jobs inside this stage are building images with the previous artifacts, making the apps portable)
    * **deploy** (Get the images previously built and deploy them in Kubernetes with right permissions and tags)
    * **destory_if_needed** (Destroy environments when needed)

## Level 1

<div align="center" width="100%">
<img src="global-assets/images/level1.png" width="50%" alt="level1">
</div>

---

- Fork the repo/ Clone the git repository on your machine
- Install the different libraries for Angular / Spring
- Customize frontend/backend for you local execution
- Test/Expose frontend/backend on your localhost

## Level 2

<div align="center" width="100%">
<img src="global-assets/images/level2.png" width="50%" alt="level2">
</div>

---

- Add and configure 2 types of runners for your project
    * One for the building of images that we called : **runner-build-3tier**
    * One for hosting kubernetes cluster : **runner-deploy-3tier** 
    > 💡 Minikube is your best friend
- Write the appropriate configuration for UAT deployment in gitlab-ci.yml : 
    * Build applications
    * Create two images (backend/frontend) based on artifacts and publish in the gitlab repository.
    > 💡 Have a look on Kaniko
    * Deploy the backend/frontend ( ❗no localhost communication accepted between ) in kubernetes using the images

## Level 3

<div align="center" width="100%">
<img src="global-assets/images/level3.png" width="50%" alt="level3">
</div>

---

- Define **3 kubernetes namespaces** for each deployment (dev/uat/prod) : You're going to separate deployment in different namespaces in kubernetes cluster for the different PROD/UAT/DEV environnements.
- Write the appropriate **configuration** for dev/uat/prod deployment in **gitlab-ci.yml**.
- Try to create a lifecycle with different **branches** as following : 
    * main branch with tagging action ($CI_COMMIT_TAG =~ /[d+].[d+].[d+]/) => PROD deployment 
    * develop branch with tagging action ($CI_COMMIT_TAG =~ /[d+].[d+].[d+]-rc*/) => UAT deployment 
    * develop branch commit without tag => DEV deployment


## Level 4

<div align="center" width="100%">
<img src="global-assets/images/level4.png" width="50%" alt="level4">
</div>

---

- The pipeline does not need to need triggered when the **README** is altered.
- Extract the DB configuration from backend files to be customized in **gitlab-ci.yml**.

## Master level

<div align="center" width="100%">
<img src="global-assets/images/level5.png" width="50%" alt="level5">
</div>

---

- Expose your application on the internet
- Deploy the landingzone ( runners / your own DB ) with terraform and the provider of your choice ( Use another git repository ).


## Where is the DB ?

> 💡 It's for the moment hosted on AWS POC account with low data inside. It's a managed Postgrl RDS.
> Endpoint : beers.cjmvufsydzlb.eu-west-3.rds.amazonaws.com
> Username : postgres
> Password : Devops-2k24